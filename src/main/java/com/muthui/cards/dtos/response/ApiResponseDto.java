package com.muthui.cards.dtos.response;

import lombok.Builder;

import java.io.Serializable;

@Builder
public record ApiResponseDto (
int responseCode,
String responseMessage,
ResultDTO result
) implements Serializable {

}