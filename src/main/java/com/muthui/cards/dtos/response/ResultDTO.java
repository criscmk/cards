package com.muthui.cards.dtos.response;

import java.io.Serializable;

public record ResultDTO (
String message,
String timestamp,
long totalRecords,
Object data) implements Serializable { }



