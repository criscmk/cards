package com.muthui.cards.dtos.response;

import lombok.*;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CardsResponse implements Serializable {
    private long id;
    private String name;
    private String color;
    private String description;


}
