package com.muthui.cards.dtos.requests.security;

public enum Role {
    MEMBER, ADMIN
}