package com.muthui.cards.dtos.requests.security;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


public record AuthResponseDto(String token,long expiry) {

}
