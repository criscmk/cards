package com.muthui.cards.dtos.requests.card;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import java.io.Serializable;

public record AddCardRequestDto(
        @NotBlank(message = "name.required")
         String name,
        String description,

        @NotBlank
        @Size(min = 6, max = 6 ,message = "color should have  6 characters")
        String color

) implements Serializable { }
