package com.muthui.cards.dtos.requests.security;

import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


public record AuthRequestDto(
        @NotBlank(message = "username.required") String username, @NotBlank(message = "password.required") String password) {

}
