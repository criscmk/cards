package com.muthui.cards.dtos.requests.card;

import com.muthui.cards.validators.CardStatusEnum;
import com.muthui.cards.validators.EnumValidator;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;

import java.io.Serializable;

public record UpdateCardDetailsRequestDto(

        @NotBlank(message = "name.required")
        String name,
        String description,
        @Size(min = 6, message = "color should have at least 6 characters")
        String color,
        @EnumValidator(message = "Card Status needs to be TODO or IN_PROGRESS OR DONE", enumClazz = CardStatusEnum.class)
        String  status
) implements Serializable { }
