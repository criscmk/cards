package com.muthui.cards.configs;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
@ConfigurationProperties("cards.api")
@Getter
@Setter
public class ApplicationConfigs {

    private int successResponseCode;
    private String successResponseMessage;
    private int failureResponseCode;
    private String failureResponseMessage;
    private Integer iteration;
    private Integer keylength;
    private String secret;
    private long expiry;

}

