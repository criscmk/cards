package com.muthui.cards.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.*;

import java.util.Date;

@Entity(name = "card")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Data
public class CardEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
    private String color;
    private String description;
    private String status;
    private long createdBy;
    private Date dateCreated;

}
