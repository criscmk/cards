package com.muthui.cards.exceptions;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class CardNameExistsException extends Exception {
    private final String cardName;
    @Override
    public String getMessage() {
        return "card with name "+ cardName +" exist";
    }
}
