package com.muthui.cards.exceptions;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muthui.cards.dtos.response.ApiResponseDto;
import com.muthui.cards.utils.ResponseService;
import io.jsonwebtoken.ExpiredJwtException;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.web.reactive.error.ErrorWebExceptionHandler;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.support.WebExchangeBindException;
import org.springframework.web.server.MethodNotAllowedException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Cris
 */
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.REACTIVE)
@Slf4j
@Component
public class DefaultExceptionHandler implements ErrorWebExceptionHandler {
    private static final String EXCEPTION_MESSAGE = "default.exception.message";
    private final Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    private final ResponseService responseManager;

    public DefaultExceptionHandler(ResponseService responseManager) {
        this.responseManager = responseManager;
    }

    @NonNull
    @Override
    public Mono<Void> handle(@NonNull ServerWebExchange exchange, @NonNull Throwable ex) {
        final ServerHttpResponse response = exchange.getResponse();
        response.setStatusCode(HttpStatus.OK);
        response.getHeaders().setContentType(MediaType.APPLICATION_JSON);


         if (ex instanceof WebExchangeBindException) {
             response.setStatusCode(HttpStatus.BAD_REQUEST);
            log.error("Missing Parameters exception {}" , ex.getMessage());
            List<String> errorList = new ArrayList<>();
            for (ObjectError objectError : ( ((WebExchangeBindException) ex).getBindingResult().getAllErrors())) {
                String defaultMessage = objectError.getDefaultMessage();
                errorList.add(defaultMessage);
            }
            final Mono<DataBuffer> buffer = buildDataBuffer(
                    exchange,
                    responseManager.handleExceptionMessage(HttpStatus.BAD_REQUEST.value(),"Bad Request", errorList)
            );
            return response.writeWith(buffer);
        }

         else if (ex instanceof SocketTimeoutException) {
             response.setStatusCode(HttpStatus.REQUEST_TIMEOUT);
             log.error("SocketTimeoutException " + ex.getMessage());
             final Mono<DataBuffer> buffer = buildDataBuffer(
                     exchange,
                     responseManager.handleExceptionMessage("socket.timeout.exception.message")
             );
             return response.writeWith(buffer);
         }
         else if (ex instanceof MethodNotAllowedException) {
             response.setStatusCode(HttpStatus.METHOD_NOT_ALLOWED);
             log.error("HttpRequestMethodNotSupportedException " + ex.getMessage());
             final Mono<DataBuffer> buffer = buildDataBuffer(
                     exchange,
                     responseManager.handleExceptionMessage("request method not supported")
             );
             return response.writeWith(buffer);
         }    else if (ex instanceof ResponseStatusException) {
             response.setStatusCode(HttpStatus.NOT_FOUND);
             log.error("Url Provided is Invalid  " + ex.getMessage());
             final Mono<DataBuffer> buffer = buildDataBuffer(
                     exchange,
                     responseManager.handleExceptionMessage("Resource Not Found "+exchange.getRequest().getPath() )
             );
             return response.writeWith(buffer);
         }
         else if(ex instanceof InvalidUserCredentialsException)
         {
             logger.error("Invalid User Credentials {}",  ex.getMessage());
             response.setStatusCode(HttpStatus.UNAUTHORIZED);
             final Mono<DataBuffer> buffer = buildDataBuffer(
                     exchange,
                     responseManager.handleExceptionMessage(HttpStatus.UNAUTHORIZED.value(), ex.getLocalizedMessage(),null));
             return response.writeWith(buffer);
         }
         else if(ex instanceof ExpiredJwtException)
         {
             logger.error("Expired User Credentials {}",  ex.getMessage());
             response.setStatusCode(HttpStatus.UNAUTHORIZED);
             final Mono<DataBuffer> buffer = buildDataBuffer(
                     exchange,
                     responseManager.handleExceptionMessage(HttpStatus.UNAUTHORIZED.value(), ex.getLocalizedMessage(),null));
             return response.writeWith(buffer);
         }

         else if(ex instanceof NoRecordFoundException)
         {
             logger.error("No Record found Exception {}",  ex.getMessage());
             response.setStatusCode(HttpStatus.BAD_REQUEST);
             final Mono<DataBuffer> buffer = buildDataBuffer(
                     exchange,
                     responseManager.handleExceptionMessage(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), List.of()));
             return response.writeWith(buffer);
         }

         else if(ex instanceof CardNameExistsException)
         {
             logger.error("Card exist Exception {}",  ex.getMessage());
             response.setStatusCode(HttpStatus.BAD_REQUEST);
             final Mono<DataBuffer> buffer = buildDataBuffer(
                     exchange,
                     responseManager.handleExceptionMessage(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), List.of()));
             return response.writeWith(buffer);
         }
         else if(ex instanceof UserExistException)
         {
             logger.error("User Exists Exception {}",  ex.getMessage());
             response.setStatusCode(HttpStatus.BAD_REQUEST);
             final Mono<DataBuffer> buffer = buildDataBuffer(
                     exchange,
                     responseManager.handleExceptionMessage(HttpStatus.BAD_REQUEST.value(), ex.getLocalizedMessage(), List.of()));
             return response.writeWith(buffer);
         }


        // catch all
        logger.error(" Error Found {} {}", ex.getClass(), ex.getMessage());
         response.setStatusCode(HttpStatus.INTERNAL_SERVER_ERROR);
        final Mono<DataBuffer> buffer = buildDataBuffer(
                exchange,
                responseManager.handleExceptionMessage(EXCEPTION_MESSAGE)
        );
        return response.writeWith(buffer);
    }

    private Mono<DataBuffer> buildDataBuffer(@NonNull ServerWebExchange exchange, @NonNull ApiResponseDto responseArray) {
        return Mono.fromCallable(() -> new ObjectMapper().writeValueAsBytes(responseArray))
                .map(bytes -> exchange.getResponse().bufferFactory().wrap(bytes));
    }
}
