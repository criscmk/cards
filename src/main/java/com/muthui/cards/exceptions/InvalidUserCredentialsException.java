package com.muthui.cards.exceptions;

public class InvalidUserCredentialsException extends Exception {
    @Override
    public String getMessage() {
        return "Invalid User Credentials";
    }
}
