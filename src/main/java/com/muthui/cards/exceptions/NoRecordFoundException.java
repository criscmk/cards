package com.muthui.cards.exceptions;

public class NoRecordFoundException extends Exception{
    @Override
    public String getMessage() {
        return "no record found matching your criteria";
    }
}
