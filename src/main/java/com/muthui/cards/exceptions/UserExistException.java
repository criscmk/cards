package com.muthui.cards.exceptions;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class UserExistException extends Exception {
    private final String email;
    @Override
    public String getMessage() {
        return "user with the email address "+ email +" exist";
    }
}
