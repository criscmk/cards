package com.muthui.cards.repository;

import com.muthui.cards.dtos.response.CardsResponse;
import com.muthui.cards.entities.CardEntity;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

public interface CardRepository extends JpaRepository<CardEntity,Long> {

    CardEntity findCardEntitiesByName(String name);

   CardEntity findCardEntitiesByIdAndCreatedBy(long cardID, long userId);

    CardEntity deleteById(long cardId);


    Page<CardEntity> findAll(Example example,Pageable pageable);

    @Query(
            value = "SELECT * FROM card c WHERE c.name like %?1% and c.status like %?2% and c.color like %?3% ",
            nativeQuery = true)
    Page<CardEntity> findUserByStatusNative(String name, String status, String color, Pageable pageable);
}
