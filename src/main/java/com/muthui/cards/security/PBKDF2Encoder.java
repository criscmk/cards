package com.muthui.cards.security;

import com.muthui.cards.configs.ApplicationConfigs;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.util.Base64;

@Configuration
@RequiredArgsConstructor
public class PBKDF2Encoder implements PasswordEncoder {

private final ApplicationConfigs configs;



    @SneakyThrows
    @Override
    public String encode(CharSequence cs) {

            byte[] result = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA512")
                                            .generateSecret(new PBEKeySpec(cs.toString().toCharArray(), configs.getSecret().getBytes(), configs.getIteration(), configs.getKeylength()))
                                            .getEncoded();
            return Base64.getEncoder().encodeToString(result);

    }

    @Override
    public boolean matches(CharSequence cs, String string) {
        return encode(cs).equals(string);
    }
}
