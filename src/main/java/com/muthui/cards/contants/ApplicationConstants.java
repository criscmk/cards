package com.muthui.cards.contants;

public class ApplicationConstants {

    public static final String TODO_STATUS ="TODO";
    public static final  String ADDED_RECORD_MESSAGE ="Card Details Added Successfully";
    public static final  String UPDATED_RECORD_MESSAGE ="Card Details Updated Successfully";
    public static final  String DELETED_RECORD_MESSAGE ="Card Details Deleted Successfully";
}
