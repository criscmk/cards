package com.muthui.cards.contants;

public class RestPath {
    private RestPath(){

    }
    public static final String API_VERSION= "/api/v1/card";
    public static final String ADD_CARD= "/add";
    public static final String CARD_DETAILS_UPDATE= "/update/{cardId}";
    public static final String CARD_FETCH_ALL= "/fetch/all";
    public static final String CARD_DETAILS_FETCH= "/fetch/{cardId}";
    public static final String CARD_DETAILS_DELETE= "/delete/{cardId}";


}
