package com.muthui.cards.service;
import com.muthui.cards.configs.ApplicationConfigs;
import com.muthui.cards.dtos.user.User;
import com.muthui.cards.dtos.requests.security.AuthRequestDto;
import com.muthui.cards.dtos.requests.security.AuthResponseDto;
import com.muthui.cards.dtos.requests.security.Role;
import com.muthui.cards.exceptions.InvalidUserCredentialsException;
import com.muthui.cards.security.JWTUtil;
import com.muthui.cards.security.PBKDF2Encoder;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * This is just an example, you can load the user from the database from the repository.
 * 
 */
@Service
@RequiredArgsConstructor
@Slf4j
public class UserService {
    private final JWTUtil jwtUtil;
    private final PBKDF2Encoder passwordEncoder;
    private final ApplicationConfigs configs;
    private Map<String, User> data;

    @PostConstruct
    public void init() {
        data = new HashMap<>();

        //populating user credentials
        data.put("member@gmail.com", new User(1,"member@gmail.com", "CT8+lO2ax/BXj0NtOD9yBf3xuG8kwb7iX9KrX0WuBBo=", true, Arrays.asList(Role.MEMBER)));

        data.put("admin@gmail.com", new User(2,"admin@gmail.com", "/At3EvlGe9qOIcru3aHbOwjxYjo3GnQwiBRPyrzLM4A=", true, Arrays.asList(Role.ADMIN)));
    }

    public Mono<ResponseEntity<AuthResponseDto>> userLogin(AuthRequestDto authRequestDto) {
        return Mono.justOrEmpty(data.get(authRequestDto.username()))
                    .filter(userDetails -> passwordEncoder.encode(authRequestDto.password()).equals(userDetails.getPassword()))
                    .map(userDetails -> ResponseEntity.ok(new AuthResponseDto(jwtUtil.generateToken(userDetails),configs.getExpiry())))
                    .switchIfEmpty(Mono.error(new InvalidUserCredentialsException()));

    }
    public Mono<User> findByUsername(String username) {
        return Mono.justOrEmpty(data.get(username));
    }




}
