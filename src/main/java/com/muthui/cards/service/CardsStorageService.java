package com.muthui.cards.service;
import com.muthui.cards.dtos.response.CardsResponse;
import com.muthui.cards.entities.CardEntity;
import com.muthui.cards.repository.CardRepository;
import com.muthui.cards.utils.SharedUtilService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.annotations.CurrentTimestamp;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.util.Date;


@Service
@Slf4j
@RequiredArgsConstructor
public class CardsStorageService {
    private final CardRepository cardRepository;
    private final SharedUtilService sharedUtilService;

    Mono<CardEntity> saveCardDetails(CardEntity cardEntity)
    {
        return Mono.defer(() -> Mono.fromCallable(() -> cardRepository.save(cardEntity))
                .flatMap(Mono::justOrEmpty)
                .subscribeOn(Schedulers.boundedElastic()));
    }

    Mono<CardEntity> checkIfCardExist(String name)
    {
        log.info("fetching card using this name {}", name);
        return Mono.defer(() -> Mono.fromCallable(() -> cardRepository.findCardEntitiesByName(name))
                .flatMap(Mono::justOrEmpty)
                .subscribeOn(Schedulers.boundedElastic()));
    }

    Mono<CardEntity> findMemberCardDetailsById(long id, long userId)
    {
        log.info("fetching card using this id {} for user {} ", id,userId);
        return Mono.defer(() -> Mono.fromCallable(() -> cardRepository.findCardEntitiesByIdAndCreatedBy(id,userId)))
                .flatMap(Mono::justOrEmpty)
                .subscribeOn(Schedulers.boundedElastic());
    }
    Mono<CardEntity> deleteCardDetails(long cardId)
    {
        log.info("deleting this card details by id {}", cardId);
        return Mono.defer(() -> Mono.fromCallable(() -> cardRepository.deleteById(cardId))
                .flatMap(Mono::justOrEmpty)
                .subscribeOn(Schedulers.boundedElastic()));
    }

    Mono<Page<CardEntity>> fetchAllCards(int page, int size, String name, String color, String status, String sortBy)
    {
        var cardEntity = new CardEntity();
        cardEntity.setName(name);
        cardEntity.setColor(color);
        cardEntity.setStatus(status);

        var matcher = ExampleMatcher.matching()
                .withMatcher("name", ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())//name filter
                .withMatcher("color",ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())//color filter
                .withMatcher("status",ExampleMatcher.GenericPropertyMatchers.contains().ignoreCase())//status filter
                .withIgnorePaths("createdBy","id","description")
                .withIgnoreNullValues()
                .withNullHandler(ExampleMatcher.NullHandler.IGNORE);


        Sort sort = Sort.by(Sort.Direction.DESC, sortBy);//sort filter

        Pageable paging = PageRequest.of(page, size,sort);
        var example = Example.of(cardEntity, matcher);


        return Mono.defer(() -> Mono.fromCallable(() -> cardRepository.findUserByStatusNative(name,status,color,paging))
                .flatMap(Mono::justOrEmpty)
                .subscribeOn(Schedulers.boundedElastic()));
    }

}
