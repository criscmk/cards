package com.muthui.cards.service;
import com.google.gson.Gson;
import com.muthui.cards.contants.ApplicationConstants;
import com.muthui.cards.entities.CardEntity;
import com.muthui.cards.exceptions.CardNameExistsException;
import com.muthui.cards.exceptions.NoRecordFoundException;
import com.muthui.cards.dtos.response.ApiResponseDto;
import com.muthui.cards.dtos.requests.card.AddCardRequestDto;
import com.muthui.cards.dtos.requests.card.UpdateCardDetailsRequestDto;
import com.muthui.cards.utils.ResponseService;
import com.muthui.cards.utils.SharedUtilService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.Date;


@Slf4j
@Service
@RequiredArgsConstructor
public class CardServiceImpl implements CardService {

    private final CardsStorageService cardsStorageService;
    private final ResponseService responseService;
    private final SharedUtilService sharedUtilService;
    @Override
    public Mono<ApiResponseDto> addCardDetails(AddCardRequestDto addCardRequestDto) {
            //check if card with the same name exists
         return sharedUtilService.loggedInUseId().flatMap(user ->
                 {
                     log.info("user details {}",new Gson().toJson(user.getUserId()));
                     log.info("add card  request {}", new Gson().toJson(addCardRequestDto));

                     var userId = user.getUserId();
                     return cardsStorageService.checkIfCardExist(addCardRequestDto.name())
                             .flatMap(cardEntity -> {
                                 //card with the same name exist .
                                 if(cardEntity !=null)
                                 {
                                     return Mono.error(new CardNameExistsException(addCardRequestDto.name()));
                                 }
                                 return responseService.failureResponse("failed");
                             }).switchIfEmpty( //card details do not exist lets proceed to create the card
                                     cardsStorageService.saveCardDetails(mapDtoCardEntity(addCardRequestDto,userId))
                                             .flatMap(savedCard -> {
                                                 log.info("saved card details successfully");
                                                 return responseService.successResponse(savedCard,ApplicationConstants.ADDED_RECORD_MESSAGE);
                                             }).onErrorResume(throwable -> {
                                                 log.error("an error occurred while saving card details {}",throwable.getLocalizedMessage());
                                                 return responseService.failureResponse(throwable.getLocalizedMessage());
                                             }));
                 }
                 );



    }


    @Override
    public Mono<ApiResponseDto> fetchCardDetails(long cardId) {
        return sharedUtilService.loggedInUseId().flatMap(user -> cardsStorageService.findMemberCardDetailsById(cardId,user.getUserId())
                .flatMap(cardEntity -> responseService.successResponse(cardEntity,1)).switchIfEmpty(Mono.error(new NoRecordFoundException())));

    }

    /**
     * Fetch all cards
     *
     * @param page
     * @param size
     * @return
     */
    @Override
    public Mono<ApiResponseDto> fetchAllCards(int page, int size,String name,String color,String status,String sortColumn) {
        //let's find card details using supplied criteria
        return cardsStorageService.fetchAllCards(page,size, name,color,status,sortColumn).flatMap(cardEntities -> {
            if(cardEntities.isEmpty())
            {
                //throw exception if no record is found
               return Mono.error(new NoRecordFoundException());
            }
            log.info("fetched details successfully");
          return   responseService.successResponse(cardEntities.get(),cardEntities.getTotalElements());
        });
    }

    /**
     * @param updateCardDetailsRequestDto
     * @return
     */
    @Override
    public Mono<ApiResponseDto> updateCardDetails(long cardId, UpdateCardDetailsRequestDto updateCardDetailsRequestDto) {
        return sharedUtilService.loggedInUseId().flatMap(user ->
                cardsStorageService.findMemberCardDetailsById(cardId, user.getUserId())
                        .flatMap(cardEntity -> {

                            cardEntity.setName(updateCardDetailsRequestDto.name());
                            cardEntity.setColor(updateCardDetailsRequestDto.color());
                            cardEntity.setDescription(updateCardDetailsRequestDto.description());
                            cardEntity.setStatus(updateCardDetailsRequestDto.status() == null ? cardEntity.getStatus() : updateCardDetailsRequestDto.status());

                            return cardsStorageService.saveCardDetails(cardEntity).flatMap(cardEntity1 -> {
                                log.info("updated card details successfully");
                                return responseService.successResponse(cardEntity,ApplicationConstants.UPDATED_RECORD_MESSAGE);
                            });
                        }).switchIfEmpty(Mono.error(new NoRecordFoundException())));

    }

    @Override
    public Mono<ApiResponseDto> deleteCardDetails(long cardId) {
        //deleting card details
        return sharedUtilService.loggedInUseId().flatMap(user ->
                cardsStorageService.findMemberCardDetailsById(cardId,user.getUserId())
                        .flatMap(entity -> cardsStorageService.deleteCardDetails(cardId).flatMap(cardEntity ->
                                        responseService.failureResponse("failed to deleted card details"))
                                .switchIfEmpty(responseService.successResponse(null,ApplicationConstants.DELETED_RECORD_MESSAGE))).switchIfEmpty(Mono.error(new NoRecordFoundException())));
    }

    private CardEntity mapDtoCardEntity(AddCardRequestDto addCardRequestDto , Long createdBy )
    {
      CardEntity cardEntity =new  CardEntity();
                cardEntity.setColor(addCardRequestDto.color());
                cardEntity.setName(addCardRequestDto.name());
                cardEntity.setStatus(ApplicationConstants.TODO_STATUS);
                cardEntity.setDescription(addCardRequestDto.description());
                cardEntity.setCreatedBy(createdBy);
                cardEntity.setDateCreated(new Date());
                return cardEntity;
    }

}
