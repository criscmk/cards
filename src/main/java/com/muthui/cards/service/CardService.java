package com.muthui.cards.service;


import com.muthui.cards.dtos.response.ApiResponseDto;
import com.muthui.cards.dtos.requests.card.AddCardRequestDto;
import com.muthui.cards.dtos.requests.card.UpdateCardDetailsRequestDto;
import reactor.core.publisher.Mono;

public interface CardService {

    /**
     * Add card details
     * @param addCardRequestDto
     * @return
     */
    Mono<ApiResponseDto> addCardDetails(AddCardRequestDto addCardRequestDto);

    /**
     * Fetch card details
     * @param cardId
     * @return
     */
    Mono<ApiResponseDto> fetchCardDetails(long cardId);

    /**
     * Fetch all cards
     * @param page
     * @param size
     * @return
     */
    Mono<ApiResponseDto> fetchAllCards(int page, int size, String name, String color, String status, String sortColumn);

    /**
     * Update card details
     * @param cardId
     * @param updateCardDetailsRequestDto
     * @return
     */
    Mono<ApiResponseDto> updateCardDetails(long cardId, UpdateCardDetailsRequestDto updateCardDetailsRequestDto);

    /**
     * Delete Card Details
     * @param cardId
     * @return
     */
    Mono<ApiResponseDto> deleteCardDetails(long cardId);

}
