package com.muthui.cards.utils;

import com.google.gson.Gson;
import com.muthui.cards.configs.ApplicationConfigs;
import com.muthui.cards.dtos.response.ApiResponseDto;
import com.muthui.cards.dtos.response.ResultDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ResponseService {

    private final ApplicationConfigs applicationConfigs;
    private final SharedUtilService sharedUtilService;
    public Mono<ApiResponseDto> successResponse(Object res, String message){
        //handle all success response
        var response = ApiResponseDto
                .builder()
                .responseCode(applicationConfigs.getSuccessResponseCode())
                .responseMessage(message)
                .result(new ResultDTO(applicationConfigs.getSuccessResponseMessage(),sharedUtilService.formattedDate(),0, res))
                .build();

        return Mono.just(response);
    }
    public Mono<ApiResponseDto> successResponse(Object res, long total){
        //handle all success response
        var response = ApiResponseDto
                .builder()
                .responseCode(applicationConfigs.getSuccessResponseCode())
                .responseMessage(applicationConfigs.getSuccessResponseMessage())
                .result(new ResultDTO(applicationConfigs.getSuccessResponseMessage(),sharedUtilService.formattedDate(),total, res))
                .build();

        return Mono.just(response);
    }

    public Mono<ApiResponseDto> failureResponse(String message){
        //handles all failure response
        var response = ApiResponseDto
                .builder()
                .responseCode(applicationConfigs.getFailureResponseCode())
                .responseMessage(message)
                .build();
        return Mono.just(response);
    }


    public ApiResponseDto handleExceptionMessage(int status, String message, List<?> errors)
    {
        var finalResponse = ApiResponseDto.builder()
                .responseCode(status)
                .responseMessage(message)
                .result(new ResultDTO(message, sharedUtilService.formattedDate(),0,errors)).build();
        log.info("Returning exception  response {}",new Gson().toJson(finalResponse));
        return finalResponse;
    }
    public ApiResponseDto handleExceptionMessage(String message) {

        return new ApiResponseDto(applicationConfigs.getFailureResponseCode(),message,null);

    }

}
