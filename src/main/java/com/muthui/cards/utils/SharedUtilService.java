package com.muthui.cards.utils;


import com.muthui.cards.dtos.user.User;
import com.muthui.cards.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.ReactiveSecurityContextHolder;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Slf4j
@RequiredArgsConstructor
@Component
public class SharedUtilService {

    private final UserService userService;


    public  final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

    public   String formattedDate()
    {
        Date date = new Date();
        DateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);
        return dateFormat.format(date);
    }


    public Mono<User> loggedInUseId() {
        log.info("fetching logged in user details");
       return ReactiveSecurityContextHolder.getContext().flatMap(securityContext -> {
            var userName = securityContext.getAuthentication().getName();
            return userService.findByUsername(userName)
                    .map(user -> user);
        });
    }



}
