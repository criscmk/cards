package com.muthui.cards.validators;

public enum CardStatusEnum {

        TODO,
        IN_PROGRESS,
        DONE
}
