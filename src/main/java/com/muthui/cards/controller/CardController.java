package com.muthui.cards.controller;
import com.muthui.cards.contants.RestPath;
import com.muthui.cards.dtos.response.ApiResponseDto;
import com.muthui.cards.dtos.requests.card.AddCardRequestDto;
import com.muthui.cards.dtos.requests.card.UpdateCardDetailsRequestDto;
import com.muthui.cards.service.CardService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Mono;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(RestPath.API_VERSION)
public class CardController {

    private final CardService cardService;
    @PreAuthorize("hasAnyAuthority('MEMBER', 'ADMIN')")
    @PostMapping(RestPath.ADD_CARD)
    public Mono<ApiResponseDto> addCardDetails(@Valid @RequestBody AddCardRequestDto addCardRequestDto){
        return cardService.addCardDetails(addCardRequestDto);
    }
    @PreAuthorize("hasAnyAuthority('MEMBER', 'ADMIN')")
    @GetMapping(RestPath.CARD_DETAILS_FETCH)
    public  Mono<ApiResponseDto> fetchCardDetails(@PathVariable int cardId){
        return cardService.fetchCardDetails(cardId);
    }
    @PreAuthorize("hasAnyAuthority('MEMBER', 'ADMIN')")
    @DeleteMapping(RestPath.CARD_DETAILS_DELETE)
    public  Mono<ApiResponseDto> deleteCardDetails(@PathVariable int cardId){
        return cardService.deleteCardDetails(cardId);
    }
    @PreAuthorize("hasAnyAuthority('MEMBER', 'ADMIN')")
    @PutMapping(RestPath.CARD_DETAILS_UPDATE)
    public  Mono<ApiResponseDto> updateCardDetails(@PathVariable int cardId, @Valid @RequestBody UpdateCardDetailsRequestDto updateCardDetailsRequestDto){
        return cardService.updateCardDetails( cardId,updateCardDetailsRequestDto);
    }

    @GetMapping(RestPath.CARD_FETCH_ALL)
    @PreAuthorize("hasAuthority('ADMIN')")
    public Mono<ApiResponseDto> fetchAllCards(@RequestParam(defaultValue = "0") int page,
                                              @RequestParam(defaultValue = "10") int size,
                                              @RequestParam(defaultValue = "") String name,
                                              @RequestParam(defaultValue = "") String color,
                                              @RequestParam(defaultValue = "") String status,
                                              @RequestParam(defaultValue = "id") String sortColumn){

        return cardService.fetchAllCards(page,size,name, color,status,sortColumn);
    }

}
