package com.muthui.cards.controller;


import com.muthui.cards.dtos.requests.security.AuthRequestDto;
import com.muthui.cards.dtos.requests.security.AuthResponseDto;
import com.muthui.cards.service.UserService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@AllArgsConstructor
@RestController
@RequestMapping("/auth")
@Slf4j
public class AuthController {

   private final UserService userService;

    @PostMapping("/login")
    public Mono<ResponseEntity<AuthResponseDto>> login(@Valid @RequestBody AuthRequestDto authRequest) {
        log.info("authenticating user credentials");
       return userService.userLogin(authRequest);
    }

}
